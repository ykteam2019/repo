﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Yk.Web.Migrations
{
    public partial class YkProject_20190825 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Navmenu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreateDate = table.Column<DateTime>(nullable: false, defaultValue: new DateTime(2019, 8, 25, 12, 21, 39, 63, DateTimeKind.Local).AddTicks(5970)),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 35, nullable: true),
                    Url = table.Column<string>(nullable: true),
                    Controller = table.Column<string>(maxLength: 35, nullable: true),
                    Action = table.Column<string>(maxLength: 35, nullable: true),
                    TopMenu = table.Column<int>(nullable: false, defaultValue: 0),
                    Icon = table.Column<string>(maxLength: 50, nullable: true),
                    MenuIndex = table.Column<int>(nullable: false, defaultValue: 0),
                    RoleName = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Navmenu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WebApp",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreateDate = table.Column<DateTime>(nullable: false, defaultValue: new DateTime(2019, 8, 25, 12, 21, 39, 65, DateTimeKind.Local).AddTicks(4613)),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 150, nullable: true),
                    Url = table.Column<string>(maxLength: 300, nullable: true),
                    StartTime = table.Column<DateTime>(nullable: false),
                    EndTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebApp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AppLogger",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreateDate = table.Column<DateTime>(nullable: false, defaultValue: new DateTime(2019, 8, 25, 12, 21, 39, 58, DateTimeKind.Local).AddTicks(3208)),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    WebAppId = table.Column<int>(nullable: false),
                    Message = table.Column<string>(maxLength: 800, nullable: true),
                    Status = table.Column<string>(maxLength: 500, nullable: true),
                    StatusCode = table.Column<string>(maxLength: 500, nullable: true),
                    StatusDescription = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppLogger", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppLogger_WebApp_WebAppId",
                        column: x => x.WebAppId,
                        principalTable: "WebApp",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppLogger_WebAppId",
                table: "AppLogger",
                column: "WebAppId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppLogger");

            migrationBuilder.DropTable(
                name: "Navmenu");

            migrationBuilder.DropTable(
                name: "WebApp");
        }
    }
}
