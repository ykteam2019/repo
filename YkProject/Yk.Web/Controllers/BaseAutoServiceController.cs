﻿using Yk.Core.Data;
using Yk.Web.Extensions;
using Yk.Web.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc;
using Yk.Web.Framework.Controllers;
using System.Linq;
using System;
using System.Diagnostics;
using Microsoft.Extensions.Logging;

namespace Yk.Web.Controllers
{
    public partial class BaseAutoServiceController<TEntity, TModel, TService> : BaseController
        where TEntity : BaseEntity
        where TModel : BaseAppEntityModel, new()
        where TService : IServiceBase<TEntity>
    {
        private readonly TService _service;
        private readonly ILogger _logger;

        public BaseAutoServiceController(TService service, ILoggerFactory logger)
        {
            _service = service;
            this._logger = logger.CreateLogger<TService>();
        }

        [HttpGet]
        public virtual object GetById(int id)
        {
            using (RequestLogger<TService> log = new RequestLogger<TService>(_logger, Activity.Current?.Id ?? HttpContext.TraceIdentifier))
            {
                var entity = _service.GetById(id);
                var entityModel = entity.ToModel<TEntity, TModel>();
                return View(entityModel);
            }
        }

        [HttpGet]
        public virtual IActionResult Index()
        {
            using (RequestLogger<TService> log = new RequestLogger<TService>(_logger, Activity.Current?.Id ?? HttpContext.TraceIdentifier))
            {
                var languages = _service.GetAll().Where(i => !i.IsDeleted);
                var languagesModel = languages.Select(x => x.ToModel<TEntity, TModel>()).ToList();
                return View(languagesModel);
            }
        }

        [HttpGet]
        public virtual ActionResult Create(int id)
        {
            using (RequestLogger<TService> log = new RequestLogger<TService>(_logger, Activity.Current?.Id ?? HttpContext.TraceIdentifier))
            {
                var entity = _service.GetById(id);

                if (entity != null)
                {
                    var entityModel = entity.ToModel<TEntity, TModel>();
                    return View(entityModel);
                }
                else
                {
                    return View(new TModel());
                }
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create(int id, TModel model)
        {
            using (RequestLogger<TService> log = new RequestLogger<TService>(_logger, Activity.Current?.Id ?? HttpContext.TraceIdentifier))
            {
                if (ModelState.IsValid)
                {
                    var current = _service.GetById(id);
                    var entity = model.ToEntity<TEntity, TModel>();
                    if (id > 0 && current != null)
                    {
                        entity = model.ToEntity(current);
                        entity.ModifiedDate = DateTime.Now;
                        _service.Update(entity);


                    }
                    else
                    {
                        entity.CreateDate = DateTime.Now;
                        _service.Insert(entity);
                    }
                }
                else
                {
                    return View(model);
                }

                var paths = Request.Path.ToString().Split('/');
                return Redirect($"/{paths[1]}/Create");
            }
        }

        [HttpGet]
        public virtual IActionResult Update(int id)
        {
            using (RequestLogger<TService> log = new RequestLogger<TService>(_logger, Activity.Current?.Id ?? HttpContext.TraceIdentifier))
            {
                var entity = _service.GetById(id);
                var entityModel = entity.ToModel<TEntity, TModel>();
                return View(entityModel);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual IActionResult Update(TModel model)
        {
            using (RequestLogger<TService> log = new RequestLogger<TService>(_logger, Activity.Current?.Id ?? HttpContext.TraceIdentifier))
            {
                if (ModelState.IsValid)
                {
                    var entity = _service.GetById(model.Id);
                    if (entity == null)
                        return View(model);

                    entity = model.ToEntity(entity);
                    _service.Update(entity);
                }
                return View(model);
            }
        }

        public virtual IActionResult Delete(int id)
        {
            using (RequestLogger<TService> log = new RequestLogger<TService>(_logger, Activity.Current?.Id ?? HttpContext.TraceIdentifier))
            {
                var entity = _service.GetById(id);
                if (entity == null)
                    return View(id);

                _service.Delete(entity);

                var paths = Request.Path.ToString().Split('/');
                return Redirect($"/{paths[1]}/Create");
            }
        }

    }
}