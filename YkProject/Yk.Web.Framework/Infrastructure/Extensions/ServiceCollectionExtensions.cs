﻿using System;
using Yk.Core.Configuration;
using Yk.Core.Engine;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using Yk.Data;
using Yk.Utility.Letter;
using Yk.Web.Framework.Identity;
using Microsoft.AspNetCore.Identity;

using System.Globalization;

namespace Yk.Web.Framework.Infrastructure.Extensions
{
    public static class ServiceCollectionExtensions
    {
        #region Utils

        private static void ConfigureStartupConfig<TConfig>(this IServiceCollection services,
            IConfiguration configuration) where TConfig : class, new()
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            var config = new TConfig();

            configuration.Bind(config);

            services.AddSingleton(config);
        }

        private static void AddHttpContextAccessor(this IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        #endregion

        public static IServiceProvider ConfigureServices(this IServiceCollection services,
            IConfigurationRoot configuration)
        {
            //services.AddIdentity<ApplicationUser, IdentityRole<int>>()
            //           .AddEntityFrameworkStores<ApplicationIdentityDbContext>().AddDefaultTokenProviders();

            services.ConfigureStartupConfig<AppConfig>(configuration.GetSection("App"));
            CultureInfo.CurrentCulture = new CultureInfo("tr-TR");
     
            configuration.EmailConfigure(); 
            //services.AddDbContext<ApplicationIdentityDbContext, ApplicationIdentityDbContext>(options => options.UseSqlServer(configuration.GetSection("App")["DataConnectionString"], m => m.MigrationsAssembly("Yk.Web")));

            services.AddDbContext<EfDbContext, EfDbContext>(options => options.UseSqlServer(configuration.GetSection("App")["DataConnectionString"], m => m.MigrationsAssembly("Yk.Web")));
        
            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/Login/Login";
                options.AccessDeniedPath = "/Login/AccessDenied";
                options.ExpireTimeSpan = TimeSpan.FromDays(3); 
                options.LogoutPath = $"/Login/Logout";
 
                options.SlidingExpiration = true;
            });


            services.Configure<IdentityOptions>(options =>
            {
                // Default Password settings.
 
            

                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 0;
                options.Lockout.MaxFailedAccessAttempts = 8;

            });
 

            services.AddHttpContextAccessor();
            var engine = EngineContext.Create();
            engine.Initialize(services);
            engine.ConfigureServices(services, configuration);

            return engine.IocManager.ServiceProvider;
        }

        public static void AddAntiForgery(this IServiceCollection services)
        {
            services.AddAntiforgery(options =>
            {
                options.Cookie.Name = ".App.Antiforgery";
            });
        }

        public static void AddHttpSession(this IServiceCollection services)
        {
            services.AddSession(options =>
            {
                options.Cookie.Name = ".App.Session";
                options.Cookie.HttpOnly = true;
            });
        }

        public static void AddAppMvc(this IServiceCollection services)
        {
            //add basic MVC feature
            var mvcBuilder = services.AddMvc(); 
            //use session temp data provider
            mvcBuilder.AddSessionStateTempDataProvider();

            //MVC now serializes JSON with camel case names by default, use this code to avoid it
            mvcBuilder.AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            //add fluent validation
            mvcBuilder.AddFluentValidation(configuration => configuration.ValidatorFactoryType = typeof(AppValidatorFactory));
        }
    }
}
