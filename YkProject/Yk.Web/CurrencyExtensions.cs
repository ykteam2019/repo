﻿
using System.Globalization;
namespace Yk.Web
{
    public static class CurrencyExtensions
    {

        public static string ToTry(this decimal Price)
        {

            var formatProvider = new CultureInfo("tr-TR", false).NumberFormat;
            return string.Format("{0:C}", Price);

        }
        public static string ToTry(this string strPrice)
        {

            var formatProvider = new CultureInfo("tr-TR", false).NumberFormat;
            if (!decimal.TryParse(strPrice.Replace(".", ","), out decimal price))
            {
                price = 0;
            }

            return string.Format("{0:C}", price);
        }




    }
}
