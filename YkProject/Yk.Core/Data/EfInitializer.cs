﻿using Microsoft.EntityFrameworkCore;

namespace Yk.Core.Data
{
    public class EfInitializer
    {
        public virtual void SeedEverything(DbContext context)
        {
            context.Database.Migrate();
            context.Database.EnsureCreated();
        }
    }
}
