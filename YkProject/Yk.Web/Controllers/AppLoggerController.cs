﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Yk.Domain;
using Yk.Services.AppLoggerService;
using Yk.Web.Models;

namespace Yk.Web.Controllers
{
    public class AppLoggerController : BaseAutoServiceController<AppLogger,AppLoggerModel,IAppLoggerService>
    {
        public AppLoggerController(IAppLoggerService service, ILoggerFactory logger) : base(service, logger)
        {

        }

      
    }
}