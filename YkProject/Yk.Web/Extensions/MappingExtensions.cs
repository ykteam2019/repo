﻿using Yk.Core.Data;
using Yk.Core.Mapper;
using Yk.Web.Framework.Mvc.Models;

namespace Yk.Web.Extensions
{
    public static class MappingExtensions
    {
        public static TDestination MapTo<TSource, TDestination>(this TSource source)
        {
            return AutoMapperConfiguration.Mapper.Map<TSource, TDestination>(source);
        }

        public static TDestination MapTo<TSource, TDestination>(this TSource source, TDestination destination)
        {
            return AutoMapperConfiguration.Mapper.Map(source, destination);
        }

        #region Languages

        public static TModel ToModel<TEntity, TModel>(this TEntity entity)
             where TEntity : BaseEntity
             where TModel : BaseAppEntityModel
        {
            return entity.MapTo<TEntity, TModel>();
        }

        public static TEntity ToEntity<TEntity, TModel>(this TModel model)
            where TEntity : BaseEntity
            where TModel : BaseAppEntityModel
        {
            return model.MapTo<TModel, TEntity>();
        }

        public static TEntity ToEntity<TEntity, TModel>(this TModel model, TEntity destination)
            where TEntity : BaseEntity
            where TModel : BaseAppEntityModel
        {
            return model.MapTo(destination);
        }

        #endregion

    }
}
