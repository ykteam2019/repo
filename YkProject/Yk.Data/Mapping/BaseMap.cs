﻿using Yk.Core.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Yk.Data.Mapping
{
    public  static class BaseMap
    {
        public static void SetDefault<T>(this EntityTypeBuilder<T> entity) where T : BaseEntity
        {
            entity.ToTable(typeof(T).Name);
            entity.HasKey(i => i.Id);
            entity.Property(i => i.Id).ValueGeneratedOnAdd();
            entity.Property(i => i.CreateDate).HasDefaultValue(DateTime.Now);
            entity.Property(i => i.IsDeleted).HasDefaultValue(false);
        }



    }

  

}