﻿using Yk.Core.Infrastructure;
using Yk.Web.Framework.Infrastructure.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Yk.Web.Framework.BackgroundService;
using Yk.Web.Framework.Notification;

namespace Yk.Web.Framework.Infrastructure.Startups
{
    public class AppMvcStartup : IAppStartup
    {
        public void ConfigureServices(IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddSingleton<IHostedService, ScheduleTask>();
            services.AddSingleton<INotification, EmailNotification>();
            services.AddAppMvc();
        }

        public void Configure(IApplicationBuilder application)
        {
            application.UseAppMvc();
        }

        public int Order => 1000;
    }
}
