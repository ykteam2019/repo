﻿using Yk.Core.Caching;
using Yk.Core.Configuration;
using Yk.Core.Data;
using Yk.Core.DependencyInjection;
using Yk.Core.Reflection;
using Yk.Data;
using Autofac;
using Microsoft.EntityFrameworkCore;

namespace Yk.Web.Framework.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public int Order => 0;

        public void Register(ContainerBuilder builder, ITypeFinder typeFinder, AppConfig config)
        {
            //cache manager
            builder.RegisterType<PerRequestCacheManager>().As<ICacheManager>().InstancePerLifetimeScope();

            //data context
            var optionsBuilder = new DbContextOptionsBuilder<EfDbContext>();
            optionsBuilder.UseSqlServer(config.DataConnectionString);

            builder.Register<IDbContext>(c => new EfDbContext(optionsBuilder.Options)).InstancePerLifetimeScope();

            //repositories
            builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();



          



            //services



            //builder.RegisterType<DayService>().As<IDayService>().InstancePerLifetimeScope();
        }
    }
}
