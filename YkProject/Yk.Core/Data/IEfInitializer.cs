﻿using Microsoft.EntityFrameworkCore;

namespace Yk.Core.Data
{
    public interface IEfInitializer
    {
        void SeedEverything(DbContext context);
    }
}