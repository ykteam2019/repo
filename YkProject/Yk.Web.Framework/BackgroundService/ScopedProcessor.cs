﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Yk.Services.WebAppService;
using System.Net;
using System.Linq;
using Yk.Services.AppLoggerService;
using Yk.Domain;

namespace Yk.Web.Framework.BackgroundService
{
    public abstract class ScopedProcessor : BackgroundService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IWebAppService _webAppService;
        private IAppLoggerService _appLoggerService;
        private INotification _notification;
        private IServiceScopeFactory serviceScopeFactory;
        private IWebAppService webAppService;
        private IAppLoggerService appLoggerService;

        public ScopedProcessor(IServiceScopeFactory serviceScopeFactory, IWebAppService webAppService, IAppLoggerService appLoggerService, INotification notification) : base()
        {
            _serviceScopeFactory = serviceScopeFactory;
            _webAppService = webAppService;
            _appLoggerService = appLoggerService;
            _notification = notification;
        }



        protected override async Task Process()
        {
            var time = Convert.ToDateTime(DateTime.Now.ToString("H:mm"));
            var responseData = _webAppService.GetAll().Where(x => Convert.ToDateTime(x.StartTime.ToString("H:mm")) <= time
                                    && time <= (Convert.ToDateTime(x.EndTime.ToString("H:mm")))).ToList();
            foreach (var item in responseData)
            {
                try
                {
                    WebRequest request = WebRequest.Create(item.Url);
                    request.Credentials = CredentialCache.DefaultCredentials;
                    WebResponse response = request.GetResponse();
                    response.Close();
                }
                catch (WebException ex)
                {
                    var exception = (ex.Response as HttpWebResponse);
                    _appLoggerService.Insert(new AppLogger
                    {
                        WebAppId = item.Id,
                        StatusCode = exception?.StatusCode.ToString(),
                        StatusDescription = exception?.StatusDescription,
                        Status = ex.Status.ToString(),
                        Message = ex.Message
                    });
                    var uygulama = _webAppService.GetById(item.Id);
                    _notification.Sender($"Hata Mail", $"Uygulama Adı:{uygulama.Name} Uygulama Url:{ uygulama.Url} Hata Kodu:{ex.Message}");

                }
            }
        }

        public abstract Task ProcessInScope(IServiceProvider serviceProvider);
    }
}
