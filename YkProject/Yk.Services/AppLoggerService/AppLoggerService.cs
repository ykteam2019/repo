﻿using Yk.Core.Data;
using Yk.Data;
using Yk.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Yk.Services.AppLoggerService
{
    public class AppLoggerService : ServiceBase<AppLogger>, IAppLoggerService
    {
        private readonly IRepository<AppLogger> _depRepository;
        private readonly EfDbContext _context;
        public AppLoggerService(IRepository<AppLogger> depRepository, EfDbContext context) : base(depRepository)
        {
            _depRepository = depRepository;
            _context = context;
        }

        public override IList<AppLogger> GetAll()
        {
            var List = new List<AppLogger>(_context.Set<AppLogger>().Where(i => !i.IsDeleted).Include(i => i.WebApp).ToList());
            return List;
        }
    }
}
