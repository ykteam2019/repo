﻿
using System;
using System.Collections.Generic;
using Yk.Core.Data;

namespace Yk.Domain
{
    public class WebApp : BaseEntity
    {

        public WebApp()
        {
            AppLoggers = new HashSet<AppLogger>();
        }

        public string Name { get; set; }
        public string Url { get; set; }

        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public IEnumerable<AppLogger> AppLoggers { get; set; }


    }
}
