﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Yk.Domain;
using Yk.Services.WebAppService;
using Yk.Web.Extensions;
using Yk.Web.Models;

namespace Yk.Web.Controllers
{
    public class WebAppController : BaseAutoServiceController<WebApp, WebAppModel, IWebAppService>
    {
        private IWebAppService _webAppService;
        public WebAppController(IWebAppService service, ILoggerFactory logger) : base(service, logger)
        {

            _webAppService = service;
        } 
        public override object GetById(int id)
        {
            return Json(_webAppService.GetById(id));
        }


        public List<WebApp> List()
        {

            return _webAppService.GetAll().Where(x => !x.IsDeleted).ToList();
        }


        public JsonResult AddWeb(WebAppModel model)
        {
            var current = _webAppService.GetById(model.Id);


            var entity = model.ToEntity<WebApp, WebAppModel>();
            if (model.Id > 0 && current != null)
            {
                entity = model.ToEntity(current);
                entity.ModifiedDate = DateTime.Now;
                _webAppService.Update(entity);

            }
            else
            {
                entity.CreateDate = DateTime.Now;
                _webAppService.Insert(entity);
            }

            return Json("Ok");
        }


        public override IActionResult Delete(int id)
        {

            base.Delete(id);
            return Json("");

        }
    }
}