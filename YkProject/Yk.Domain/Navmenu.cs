﻿using Yk.Core.Data;
using System.Collections.Generic;

namespace Yk.Domain
{
    public class Navmenu : BaseEntity
    {
        public Navmenu()
        { 
        }


        public string Name { get; set; }
        public string Url { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public int TopMenu { get; set; }
        public string Icon { get; set; }
        public int MenuIndex { get; set; }
        public string RoleName { get; set; }
 
    }
}
