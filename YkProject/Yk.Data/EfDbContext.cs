﻿using System;
using System.Linq;
using Yk.Core.Data;
using Yk.Core.Engine;
using Yk.Core.Reflection;
using Microsoft.EntityFrameworkCore;

namespace Yk.Data
{
    public class EfDbContext : DbContext, IDbContext
    {
        #region Ctors

        public EfDbContext()
        {
        }

        public EfDbContext(DbContextOptions<EfDbContext> options)
            : base(options)
        {
        }
        

        #endregion

        #region Utils

        private void AddAllConfigurations(ModelBuilder modelBuilder)
        {
            var entityRegistrars = EngineContext.Current.IocManager
                .Resolve<ITypeFinder>().FindClassesOfType<IEntityRegistry>()
                .ToList();

            var instances = entityRegistrars
               .Select(mapperConfiguration => (IEntityRegistry)Activator.CreateInstance(mapperConfiguration))
               .ToList();

            foreach (var instance in instances)
            {
                instance.Register(modelBuilder);
            }
        }

        #endregion

        #region Overriding

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            AddAllConfigurations(modelBuilder);
        }

        #endregion

        #region Methods

        public new DbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity => base.Set<TEntity>();

        #endregion
    }
}
