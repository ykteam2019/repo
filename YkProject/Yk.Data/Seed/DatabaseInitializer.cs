﻿using Yk.Core.Data;
using Yk.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Yk.Data.Seed
{
    public class DatabaseInitializer : EfInitializer
    {
        public static void Initialize(DbContext context)
        {
            var initializer = new DatabaseInitializer();
            initializer.SeedEverything(context);
        }

        public override void SeedEverything(DbContext context)
        {
            base.SeedEverything(context);



            if (!context.Set<Navmenu>().Any())
            {
                context.Set<Navmenu>().AddRange(new List<Navmenu>
                {
                    new Navmenu {Name = "Anasayfa", Controller="Home", Action = "Index", Url="/home", Icon="la la-home", TopMenu=0, CreateDate = DateTime.Now, IsDeleted = false },

                    new Navmenu {Name = "Uygulama İşlemleri", Controller="WebApp", Action = "Create", Url="/WebApp/Create",Icon="la la-external-link", TopMenu=0, CreateDate = DateTime.Now, IsDeleted = false },
                          new Navmenu {Name = "Loglar", Controller="AppLogger", Action = "Index", Url="/AppLogger/Index",Icon="la la-list", TopMenu=0, CreateDate = DateTime.Now, IsDeleted = false }
                });

                context.SaveChanges();
            }


            if (!context.Set<WebApp>().Any())
            {
                context.Set<WebApp>().AddRange(new List<WebApp>
                {
                    new WebApp {Name="Google",Url="http://google.com.tr/",CreateDate=DateTime.Now,StartTime=DateTime.Now,EndTime=DateTime.Now.AddHours(5),IsDeleted=false },
                      new WebApp {Name="Google 2",Url="http://google.com.tr/asdasd",CreateDate=DateTime.Now,StartTime=DateTime.Now.AddHours(-5),EndTime=DateTime.Now.AddHours(2),IsDeleted=false },
                        new WebApp {Name="Microsoft",Url="https://www.microsoft.com/",CreateDate=DateTime.Now,StartTime=DateTime.Now.AddHours(-2),EndTime=DateTime.Now.AddHours(1),IsDeleted=false },
                          new WebApp {Name="Microsoft 2 ",Url="https://www.microsoft.com/asdasd",CreateDate=DateTime.Now,StartTime=DateTime.Now,EndTime=DateTime.Now.AddHours(5),IsDeleted=false },
                                                  new WebApp {Name="facebook",Url="https://www.facebook.com/",CreateDate=DateTime.Now,StartTime=DateTime.Now.AddHours(10),EndTime=DateTime.Now.AddHours(9),IsDeleted=false },
                          new WebApp {Name="facebook 2 ",Url="https://www.facebook.com/aaasdasd",CreateDate=DateTime.Now,StartTime=DateTime.Now,EndTime=DateTime.Now.AddHours(10),IsDeleted=false }

                });
                context.SaveChanges();
            }





        }
    }
}
