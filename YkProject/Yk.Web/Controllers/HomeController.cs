﻿using System.Threading.Tasks;
using Yk.Web.Framework.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Yk.Services.AppLoggerService;
using Yk.Services.WebAppService;
using System.Linq;
using System.Net;
using System;
using Yk.Domain;

namespace Yk.Web.Controllers
{

    public class HomeController : Controller
    {

        private IAppLoggerService _appLoggerService;
        private IWebAppService _webAppService;
        public HomeController(IAppLoggerService appLoggerService, IWebAppService webAppService)
        {
            _webAppService = webAppService;
            _appLoggerService = appLoggerService;
        }


        public async Task<IActionResult> Index()
        {
             
            return View();


        }
    }
}