﻿using Yk.Core.Configuration;
using Yk.Core.Engine;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

namespace Yk.Web.Framework.Infrastructure.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static void Configure(this IApplicationBuilder application)
        {
            EngineContext.Current.Configure(application);
        }

        public static void UseAppMvc(this IApplicationBuilder application)
        {
            //Startup
 


            application.UseAuthentication();
            application.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            application.UseMvcWithDefaultRoute();
            application.UseStatusCodePages();
           application.UseAuthentication();




        }

        public static void UseAppExceptionHandler(this IApplicationBuilder application)
        {
            var appConfig = EngineContext.Current.IocManager.Resolve<AppConfig>();
            var hostingEnvironment = EngineContext.Current.IocManager.Resolve<IHostingEnvironment>();
            var useDetailedExceptionPage = appConfig.DisplayFullErrorStack || hostingEnvironment.IsDevelopment();
            application.UseStatusCodePagesWithReExecute("/Login/NotFound/{0}");

            //if (!env.IsDevelopment())
            //{
                application.UseExceptionHandler("/Login/NotFound/{0}");
            ////}
            if (useDetailedExceptionPage)
            {
                //get detailed exceptions for developing and testing purposes
                application.UseDeveloperExceptionPage();
            }
            else
            {
                //or use special exception handler
                application.UseExceptionHandler("/Login/NotFound/{0}");
            }

            //log errors
        }
    }
}
