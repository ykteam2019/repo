﻿using Microsoft.Extensions.Configuration;
using System;

namespace Yk.Utility.Letter
{
    public static class EmailConfiguration
    {
        internal static string GroupEmail { get; set; }
        internal static string SmtpServer { get; set; }
        internal static bool EnableSsl { get; set; }
        internal static int SmtpPort { get; set; }
        internal static string MailTitle { get; set; }
        internal static string SmtpUser { get; set; }
        internal static string SmtpPass { get; set; }

        internal static string To { get; set; }

        public static void EmailConfigure(this IConfiguration Configuration)
        {
            try
            {
                var section = Configuration.GetSection("Email");

                SmtpServer = section["SmtpServer"];
                EnableSsl = Convert.ToBoolean(section["EnableSsl"]);
                SmtpPort = Convert.ToInt32(section["SmtpPort"]);
                MailTitle = section["MailTitle"];
                SmtpUser = section["SmtpUser"];
                SmtpPass = section["SmtpPass"];
                To = section["To"];


                if (string.IsNullOrEmpty(SmtpServer))
                {
                    SmtpServer = "smtp.gmail.com";
                    EnableSsl = true;
                    SmtpPort = 587;
                    MailTitle = "  AUTO MAIL";
                    SmtpUser = "yasink@gmail.com";
                    SmtpPass = "123456";
                    To = "yasin-kizilbakir@hotmail.com.tr";
                }
            }
            catch
            {
            }
        }
    }
}
