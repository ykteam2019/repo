﻿using Autofac;
using Yk.Core.Reflection;
using Yk.Core.Configuration;

namespace Yk.Core.DependencyInjection
{
    public interface IDependencyRegistrar
    {
        void Register(ContainerBuilder builder, ITypeFinder typeFinder, AppConfig config);

        int Order { get; } 
    }
}
