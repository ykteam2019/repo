﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;
using Yk.Services.AppLoggerService;
using Yk.Services.WebAppService;

namespace Yk.Web.Framework.BackgroundService
{
    public class ScheduleTask : ScheduledProcessor
    {
        IWebAppService _webAppService;
        IAppLoggerService _appLoggerService;
      

        public ScheduleTask(IServiceScopeFactory serviceScopeFactory, IWebAppService webAppService, IAppLoggerService appLoggerService, INotification notification) : base(serviceScopeFactory, webAppService, appLoggerService, notification)
        {

        }
         
        //5 dakika
        protected override string Schedule => "*/20 * * * *";

        public override Task ProcessInScope(IServiceProvider serviceProvider)
        {
           
            return Task.CompletedTask;
        }
    }
}
