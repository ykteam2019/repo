﻿using Yk.Web.Framework.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yk.Web.Models
{
    public class AppLoggerModel : BaseAppEntityModel
    {
        public AppLoggerModel()
        {
       
        }
        public int WebAppId { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public string StatusCode { get; set; }
        public string StatusDescription { get; set; }


    }
}
