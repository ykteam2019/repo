﻿using Yk.Core.Data;
using Yk.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yk.Data.Mapping
{
    public class AppLoggerMap : IEntityRegistry
    {
        public void Register(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppLogger>(entity =>
            {
                entity.SetDefault();
                entity.Property(x => x.Message).HasMaxLength(800);
                entity.Property(x => x.Status).HasMaxLength(500);
                entity.Property(x => x.StatusCode).HasMaxLength(500);
                entity.Property(x => x.StatusDescription).HasMaxLength(1000);
                entity.HasOne(x => x.WebApp).WithMany(i => i.AppLoggers).HasForeignKey(i => i.WebAppId);
            });
        }
    }
}
