﻿using Yk.Core.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace Yk.Web.Framework.Infrastructure.Startups
{
    public class AuthenticationStartup : IAppStartup
    {
        public void ConfigureServices(IServiceCollection services, IConfigurationRoot configuration)
        {


 

            ////add data protection
            //services.AddNopDataProtection();

            ////add authentication
            //services.AddNopAuthentication();

            //services.AddDbContext<ApplicationIdentityDbContext>(options=>opt)

            //services.AddDbContext<ApplicationIdentityDbContext, EfDbContext>(options => options.UseSqlServer(configuration.GetSection("App")["DataConnectionString"], m => m.MigrationsAssembly("Yk.Web")));

        }

        public void Configure(IApplicationBuilder application)
        {
            ////configure authentication
            //application.UseNopAuthentication();

            ////set request culture
            //application.UseCulture(); 
        }

        public int Order => 500;
    }
}
