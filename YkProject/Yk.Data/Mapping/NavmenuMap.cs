﻿using Yk.Core.Data;
using Yk.Domain;
using Microsoft.EntityFrameworkCore;

namespace Yk.Data.Mapping
{
    public class NavmenuMap : IEntityRegistry
    {
        public void Register(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Navmenu>(entity =>
            {

                entity.SetDefault();
                entity.Property(i => i.Name).HasMaxLength(35);
                entity.Property(i => i.Controller).HasMaxLength(35);
                entity.Property(i => i.Action).HasMaxLength(35);
                entity.Property(i => i.TopMenu).HasDefaultValue(0);
                entity.Property(i => i.Icon).HasMaxLength(50);
                entity.Property(i => i.MenuIndex).HasDefaultValue(0);
                entity.Property(i => i.RoleName).HasMaxLength(20);



            });
        }
    }
}
