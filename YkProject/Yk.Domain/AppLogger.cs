﻿
using Yk.Core.Data;

namespace Yk.Domain
{
    public class AppLogger : BaseEntity
    {
        public int WebAppId { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public string StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public WebApp WebApp { get; set; }

    }
}
