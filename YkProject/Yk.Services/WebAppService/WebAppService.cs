﻿using System;
using System.Collections.Generic;
using System.Text;
using Yk.Core.Data;
using Yk.Data;
using Yk.Domain;

namespace Yk.Services.WebAppService
{
    public class WebAppService : ServiceBase<WebApp>, IWebAppService
    {
        public WebAppService(IRepository<WebApp> repository) : base(repository)
        {
        }
    }
}
