﻿using Yk.Core.Infrastructure;
using Yk.Web.Framework.Infrastructure.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Reflection;

namespace Yk.Web.Framework.Infrastructure.Startups
{
    public class AppCommonStartup : IAppStartup
    {
        public void ConfigureServices(IServiceCollection services, IConfigurationRoot configuration)
        {
 
            var all = Assembly.GetEntryAssembly()
                              .GetReferencedAssemblies()
                              .Select(Assembly.Load)
                              .SelectMany(x => x.DefinedTypes).Where(i => i.FullName.Contains("Yk.") && i.Name.Contains("Service") && i.Namespace.Contains("Yk.Services"));

            foreach (var ti in all)
            {
                if (ti.IsClass)
                {
                    var tclass = ti.AsType();
                    var tinterface = all.FirstOrDefault(i => i.Name == "I" + ti.Name).AsType();

                    if (tclass != null && tinterface != null)
                    {
                        services.AddScoped(tinterface, tclass);
                    }
                }

            }
            services.AddSingleton(configuration);


            services.AddHttpSession();

            services.AddAntiForgery();
        }

        public void Configure(IApplicationBuilder application)
        {
            application.UseStaticFiles();

            application.UseSession();
        }

        public int Order => 100;
    }
}
