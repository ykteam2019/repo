﻿namespace Yk.Core.Mapper
{
    public interface IMapperProfile
    {
        int Order { get; }
    }
}
