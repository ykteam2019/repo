﻿using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;

namespace Yk.Web.Extensions
{
    public class RequestLogger<TService> : IDisposable
    {
        private readonly Stopwatch _timer;
        private readonly ILogger _logger;
        private ILoggerFactory _logger1;
        private string v;

        public RequestLogger(ILogger logger, string RequestId)
        {
            _timer = new Stopwatch();
            _timer.Start();
            this._logger = logger;
            this.RequestId = RequestId;
        }

        public RequestLogger(ILoggerFactory logger1, string v)
        {
            _logger1 = logger1;
            this.v = v;
        }

        public string RequestId { get; }

        public void Dispose()
        {
            _timer.Stop();
            if (_timer.ElapsedMilliseconds > 500)
            {
                var name = typeof(TService).Name;

                // TODO: Add User Details

                _logger.LogWarning("FsmLogger - Long Running Request: {Name} ({ElapsedMilliseconds} milliseconds) {@Request}", name, _timer.ElapsedMilliseconds, RequestId);
            }


            GC.SuppressFinalize(this);
        }
    }
}
