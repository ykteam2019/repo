﻿using Microsoft.EntityFrameworkCore;

namespace Yk.Core.Data
{
    public interface IEntityRegistry    {
        void Register(ModelBuilder modelBuilder);
    }
}
