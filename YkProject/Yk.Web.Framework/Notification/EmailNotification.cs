﻿using System;
using System.Collections.Generic;
using System.Text;
using Yk.Utility.Letter;

namespace Yk.Web.Framework.Notification
{
    public class EmailNotification : INotification
    {
        public void Sender(string Subject, string Message)
        {
            EmailSender.Send(Subject, Message);
        }
    }
}
