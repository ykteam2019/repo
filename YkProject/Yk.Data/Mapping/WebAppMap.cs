﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Yk.Core.Data;
using Yk.Domain;

namespace Yk.Data.Mapping
{
    public class WebAppMap : IEntityRegistry
    {
        public void Register(ModelBuilder modelBuilder)
        {
        


            modelBuilder.Entity<WebApp>(entity =>
            {
                entity.SetDefault();
                entity.Property(x => x.Name).HasMaxLength(150);
                entity.Property(x => x.Url).HasMaxLength(300);
               
                // Default  value 20 minute check web app

            });
        }
    }
}
