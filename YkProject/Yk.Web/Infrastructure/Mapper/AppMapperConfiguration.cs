﻿using AutoMapper;
using Yk.Core.Mapper;
using Yk.Domain;
using Yk.Web.Models;

namespace Yk.Web.Infrastructure.Mapper
{
    public class AppMapperConfiguration : Profile, IMapperProfile
    {
        public int Order => 0;

        public AppMapperConfiguration()
        {
            
           
            CreateMap<AppLogger, AppLoggerModel>()
 .ForMember(dest => dest.CustomProperties, mo => mo.Ignore());
            CreateMap<AppLoggerModel, AppLogger>();

            CreateMap<WebApp, WebAppModel>()
.ForMember(dest => dest.CustomProperties, mo => mo.Ignore());
            CreateMap<WebAppModel, WebApp>();


        }
    }
}
