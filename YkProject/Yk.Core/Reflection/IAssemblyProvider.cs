﻿namespace Yk.Core.Reflection
{
    using System.Collections.Generic;
    using System.Reflection;

    public interface IAssemblyProvider
    {
        IEnumerable<Assembly> GetAssemblies();
    }
}