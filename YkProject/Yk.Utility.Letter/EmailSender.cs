﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading;

namespace Yk.Utility.Letter
{
    public class EmailSender
    {
        public static void Send(/*string To,*/ string mailSubject, string HtmlMessage, string cc = "", string bcc = "", string replyto = "", string Title = "")
        {
            var mailTitle = string.IsNullOrWhiteSpace(Title) ? EmailConfiguration.MailTitle : Title;
            SendMail(HtmlMessage, mailSubject, mailTitle/*, To*/, cc, bcc, replyto);
        }

        private static void SendMail(string htmlMessage, string mailSubject, string mailTitle/*, string to*/, string cc, string bcc, string replyto)
        {
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(EmailConfiguration.SmtpUser, mailTitle);

                foreach (var t in EmailConfiguration.To.Split(','))
                {
                    mailMessage.To.Add(t);
                }

                if (!string.IsNullOrEmpty(replyto))
                {
                    foreach (var t in replyto.Split(','))
                    {
                        mailMessage.ReplyToList.Add(t);
                    }
                }

                if (!string.IsNullOrEmpty(cc))
                {
                    foreach (var t in cc.Split(','))
                    {
                        mailMessage.CC.Add(t);
                    }
                }

                if (!string.IsNullOrEmpty(bcc))
                {
                    foreach (var t in bcc.Split(','))
                    {
                        mailMessage.Bcc.Add(t);
                    }
                }


                mailMessage.Body = $"<body style='  font-family:sans-serif; font-size:12px; background-position: right bottom, left top; background-repeat:no-repeat,repeat; padding:10px 10px 10px 10px; margin-top:5px; margin-bottom:20px;' width='100%' height='100%'><div style='background-color:#ccc; margin-left:auto; margin-right:auto; width:700px; border:1px solid #eee; border-radius:10px; padding:10px 10px 10px 10px; margin-top:2%;'><div id='header' style='width:700px; text-align:center;'> </div><hr /><div id='content' style='border:1px solid gray; padding-bottom:25px; width:100%; position:relative; z-index:9999; margin-left:0px; background-color:#eee;  text-align:left;'><br> {htmlMessage} </div><hr/> </div><br /><br/></body>";
                mailMessage.IsBodyHtml = true;
                mailMessage.Subject = mailSubject;
                new Thread(new ThreadStart(() => SendMailThread(mailMessage))).Start();

            }
            catch (Exception exp)
            {
                throw new Exception("Beklenmeyen bir hata ile karşılaşıldı!" + exp.Message);
            }
        }

        private static void SendMailThread(MailMessage message)
        {
            using (SmtpClient client = new SmtpClient(EmailConfiguration.SmtpServer, EmailConfiguration.SmtpPort))
            {
                client.UseDefaultCredentials = false;
                client.EnableSsl = EmailConfiguration.EnableSsl;
                client.Credentials = new NetworkCredential(EmailConfiguration.SmtpUser, EmailConfiguration.SmtpPass);

                try
                {
                    client.Send(message);
                }
                catch (Exception exp)
                {
                    throw new Exception("Beklenmeyen bir hata ile karşılaşıldı!" + exp.Message);
                }
            }
        }
    }
}
