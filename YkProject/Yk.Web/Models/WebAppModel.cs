﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yk.Web.Framework.Mvc.Models;

namespace Yk.Web.Models
{
    public class WebAppModel:BaseAppEntityModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public int Minutely { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}
