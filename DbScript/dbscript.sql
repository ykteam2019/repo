USE [master]
GO
/****** Object:  Database [YkProjectDB]    Script Date: 25.08.2019 12:31:30 ******/
CREATE DATABASE [YkProjectDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'YkProjectDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\YkProjectDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'YkProjectDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\YkProjectDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [YkProjectDB] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [YkProjectDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [YkProjectDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [YkProjectDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [YkProjectDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [YkProjectDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [YkProjectDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [YkProjectDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [YkProjectDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [YkProjectDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [YkProjectDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [YkProjectDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [YkProjectDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [YkProjectDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [YkProjectDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [YkProjectDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [YkProjectDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [YkProjectDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [YkProjectDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [YkProjectDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [YkProjectDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [YkProjectDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [YkProjectDB] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [YkProjectDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [YkProjectDB] SET RECOVERY FULL 
GO
ALTER DATABASE [YkProjectDB] SET  MULTI_USER 
GO
ALTER DATABASE [YkProjectDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [YkProjectDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [YkProjectDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [YkProjectDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [YkProjectDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'YkProjectDB', N'ON'
GO
ALTER DATABASE [YkProjectDB] SET QUERY_STORE = OFF
GO
USE [YkProjectDB]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [YkProjectDB]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 25.08.2019 12:31:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppLogger]    Script Date: 25.08.2019 12:31:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppLogger](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreateDate] [datetime2](7) NOT NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[WebAppId] [int] NOT NULL,
	[Message] [nvarchar](800) NULL,
	[Status] [nvarchar](500) NULL,
	[StatusCode] [nvarchar](500) NULL,
	[StatusDescription] [nvarchar](1000) NULL,
 CONSTRAINT [PK_AppLogger] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Navmenu]    Script Date: 25.08.2019 12:31:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Navmenu](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreateDate] [datetime2](7) NOT NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[Name] [nvarchar](35) NULL,
	[Url] [nvarchar](max) NULL,
	[Controller] [nvarchar](35) NULL,
	[Action] [nvarchar](35) NULL,
	[TopMenu] [int] NOT NULL,
	[Icon] [nvarchar](50) NULL,
	[MenuIndex] [int] NOT NULL,
	[RoleName] [nvarchar](20) NULL,
 CONSTRAINT [PK_Navmenu] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WebApp]    Script Date: 25.08.2019 12:31:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebApp](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreateDate] [datetime2](7) NOT NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[Name] [nvarchar](150) NULL,
	[Url] [nvarchar](300) NULL,
	[StartTime] [datetime2](7) NOT NULL,
	[EndTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_WebApp] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190825092139_YkProject_20190825', N'2.2.4-servicing-10062')
SET IDENTITY_INSERT [dbo].[Navmenu] ON 

INSERT [dbo].[Navmenu] ([Id], [IsDeleted], [CreateDate], [ModifiedDate], [Name], [Url], [Controller], [Action], [TopMenu], [Icon], [MenuIndex], [RoleName]) VALUES (1, 0, CAST(N'2019-08-25T12:22:14.9092260' AS DateTime2), NULL, N'Anasayfa', N'/home', N'Home', N'Index', 0, N'la la-home', 0, NULL)
INSERT [dbo].[Navmenu] ([Id], [IsDeleted], [CreateDate], [ModifiedDate], [Name], [Url], [Controller], [Action], [TopMenu], [Icon], [MenuIndex], [RoleName]) VALUES (2, 0, CAST(N'2019-08-25T12:22:14.9094071' AS DateTime2), NULL, N'Uygulama İşlemleri', N'/WebApp/Create', N'WebApp', N'Create', 0, N'la la-external-link', 0, NULL)
INSERT [dbo].[Navmenu] ([Id], [IsDeleted], [CreateDate], [ModifiedDate], [Name], [Url], [Controller], [Action], [TopMenu], [Icon], [MenuIndex], [RoleName]) VALUES (3, 0, CAST(N'2019-08-25T12:22:14.9094077' AS DateTime2), NULL, N'Loglar', N'/AppLogger/Index', N'AppLogger', N'Index', 0, N'la la-list', 0, NULL)
SET IDENTITY_INSERT [dbo].[Navmenu] OFF
SET IDENTITY_INSERT [dbo].[WebApp] ON 

INSERT [dbo].[WebApp] ([Id], [IsDeleted], [CreateDate], [ModifiedDate], [Name], [Url], [StartTime], [EndTime]) VALUES (1, 0, CAST(N'2019-08-25T12:22:15.0992786' AS DateTime2), NULL, N'Google', N'http://google.com.tr/', CAST(N'2019-08-25T12:22:15.0992798' AS DateTime2), CAST(N'2019-08-25T17:22:15.0994033' AS DateTime2))
INSERT [dbo].[WebApp] ([Id], [IsDeleted], [CreateDate], [ModifiedDate], [Name], [Url], [StartTime], [EndTime]) VALUES (2, 0, CAST(N'2019-08-25T12:22:15.0995419' AS DateTime2), NULL, N'Google 2', N'http://google.com.tr/asdasd', CAST(N'2019-08-25T07:22:15.0995423' AS DateTime2), CAST(N'2019-08-25T14:22:15.0995427' AS DateTime2))
INSERT [dbo].[WebApp] ([Id], [IsDeleted], [CreateDate], [ModifiedDate], [Name], [Url], [StartTime], [EndTime]) VALUES (3, 0, CAST(N'2019-08-25T12:22:15.0995431' AS DateTime2), NULL, N'Microsoft', N'https://www.microsoft.com/', CAST(N'2019-08-25T10:22:15.0995434' AS DateTime2), CAST(N'2019-08-25T13:22:15.0995436' AS DateTime2))
INSERT [dbo].[WebApp] ([Id], [IsDeleted], [CreateDate], [ModifiedDate], [Name], [Url], [StartTime], [EndTime]) VALUES (4, 0, CAST(N'2019-08-25T12:22:15.0995438' AS DateTime2), NULL, N'Microsoft 2 ', N'https://www.microsoft.com/asdasd', CAST(N'2019-08-25T12:22:15.0995441' AS DateTime2), CAST(N'2019-08-25T17:22:15.0995443' AS DateTime2))
INSERT [dbo].[WebApp] ([Id], [IsDeleted], [CreateDate], [ModifiedDate], [Name], [Url], [StartTime], [EndTime]) VALUES (5, 0, CAST(N'2019-08-25T12:22:15.0995446' AS DateTime2), NULL, N'facebook', N'https://www.facebook.com/', CAST(N'2019-08-25T22:22:15.0995450' AS DateTime2), CAST(N'2019-08-25T21:22:15.0995451' AS DateTime2))
INSERT [dbo].[WebApp] ([Id], [IsDeleted], [CreateDate], [ModifiedDate], [Name], [Url], [StartTime], [EndTime]) VALUES (6, 0, CAST(N'2019-08-25T12:22:15.0995460' AS DateTime2), NULL, N'facebook 2 ', N'https://www.facebook.com/aaasdasd', CAST(N'2019-08-25T12:22:15.0995464' AS DateTime2), CAST(N'2019-08-25T22:22:15.0995465' AS DateTime2))
SET IDENTITY_INSERT [dbo].[WebApp] OFF
/****** Object:  Index [IX_AppLogger_WebAppId]    Script Date: 25.08.2019 12:31:31 ******/
CREATE NONCLUSTERED INDEX [IX_AppLogger_WebAppId] ON [dbo].[AppLogger]
(
	[WebAppId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AppLogger] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AppLogger] ADD  DEFAULT ('2019-08-25T12:21:39.0583208+03:00') FOR [CreateDate]
GO
ALTER TABLE [dbo].[Navmenu] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Navmenu] ADD  DEFAULT ('2019-08-25T12:21:39.0635970+03:00') FOR [CreateDate]
GO
ALTER TABLE [dbo].[Navmenu] ADD  DEFAULT ((0)) FOR [TopMenu]
GO
ALTER TABLE [dbo].[Navmenu] ADD  DEFAULT ((0)) FOR [MenuIndex]
GO
ALTER TABLE [dbo].[WebApp] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[WebApp] ADD  DEFAULT ('2019-08-25T12:21:39.0654613+03:00') FOR [CreateDate]
GO
ALTER TABLE [dbo].[AppLogger]  WITH CHECK ADD  CONSTRAINT [FK_AppLogger_WebApp_WebAppId] FOREIGN KEY([WebAppId])
REFERENCES [dbo].[WebApp] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AppLogger] CHECK CONSTRAINT [FK_AppLogger_WebApp_WebAppId]
GO
USE [master]
GO
ALTER DATABASE [YkProjectDB] SET  READ_WRITE 
GO
