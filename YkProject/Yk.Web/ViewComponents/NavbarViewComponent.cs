﻿using Yk.Data;
using Yk.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Yk.Web.ViewComponents
{
    [ViewComponent(Name = "Navbar")]
    public class NavbarViewComponent : ViewComponent
    {
        private readonly EfDbContext context;
 
        public NavbarViewComponent(EfDbContext context )
        {
         
            this.context = context;
        }
         
     
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var pages = await context.Set<Navmenu>().Where(i => !i.IsDeleted).OrderBy(x => x.MenuIndex).ToListAsync();

            return View(pages);
        }

    }
}
