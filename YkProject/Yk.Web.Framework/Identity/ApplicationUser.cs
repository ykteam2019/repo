﻿
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace Yk.Web.Framework.Identity
{
    public class ApplicationUser : IdentityUser<int>
    {
        [MaxLength(100)]
        public string NameSurname { get; set; }
    }
}
